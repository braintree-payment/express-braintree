# express-braintree

## Getting Started
- Install and start Redis server: localhost:6379 (required)

```
brew update
brew install redis
redis-server
```

- Start Express server: localhost:3001

```
git clone https://gitlab.com/braintree-payment/express-braintree.git
cd express-braintree
npm i
npm start
```
## Architecture
![Schema](resources/architecture.jpg)

## API documentation
- [Doc](https://documenter.getpostman.com/view/180809/braintree/77cf6KH)

## Data Structure for payment record
- [In Database](resources/schema.json)
- [Braintree payment](resources/braintree.json)
- [Paypal payment](resources/paypal.json)

## Sandbox Dashboards
* [Braintree](https://sandbox.braintreegateway.com/merchants/mny8vycxcsfqf8tm/home)
* [Paypal](https://developer.paypal.com/developer/applications) 
* [Firebase](https://console.firebase.google.com/project/hk01-14924/database/data)

## How to add additional payment gateways
- Add conditional checking on Front end
- Request two separate checkout flows
    - Braintree checkout APIs
        - /payment/client_token
        - /payment/checkout
    - Paypal checkout APIs
        - /paypal/create-payment
        - /paypal/execute/:paymentId

## How to guarantee payment record are found in our database and Paypal?
1. Try fetch from redis cache
2. Retry fetch from Firebase, Braintree, Paypal parallelly
3. Store the result on redis if the payment from Firebase, Braintree or Paypal

## Cache may expired when check order record
- Refetch the data from Firebase 
- Refetch the data from Braintree
    - Store them to cache and Firebase again
- Refetch the data from Paypal
    - Store them to cache and Firebase again

## Data encryption for the payment record query
- [crypto-js](https://github.com/brix/crypto-js)
    - AES Encryption

## References
* [paypal-overview](https://developer.paypal.com/docs/api/overview/#curl-example)
* [paypal-apis](https://developer.paypal.com/docs/api/payments/)
* [paypal-rest-api](https://github.com/paypal/paypal-checkout/blob/master/docs/paypal-rest-api.md)
* [paypal-node-SDK](http://paypal.github.io/PayPal-node-SDK/)
* [nodejs-api-redis-cache](https://coligo.io/nodejs-api-redis-cache/)
* [test-credit-cards](https://developers.braintreepayments.com/guides/credit-cards/testing-go-live/node)
* [using-redis-node-js](https://www.sitepoint.com/using-redis-node-js/)


## Notes
- Braintree and PayPal is not support CNY currency on sandbox 
![Scheme](resources/cur.png)
![Schema](resources/cur1.png)