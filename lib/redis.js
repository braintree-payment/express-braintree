import Bluebird from 'bluebird'

const redis = Bluebird.promisifyAll(require('redis'))

export const client = redis.createClient()

client.on('error', (err) => {
  console.log(`Error ${err}`)
})

/**
 *
 * @param {string} id - payment id
 * @param {object} data - response data
 * @param {integer} TTL - expiration time
 * @return {'OK'<string>}
 */
export const setPaymentByID = async (id, data, TTL) => {
  try {
    // store cache for 1 hour
    if (!TTL) {
      client.expire(id, 3600)
    } else {
      client.expire(id, TTL)
    }
    return await client.setAsync(id, JSON.stringify(data))
  } catch (err) {
    throw err
  }
}

/**
 *
 * @param {string} id
 * @return {payment<string>}
 */
export const findPayment = async (id) => {
  try {
    return await client.getAsync(id)
  } catch (err) {
    throw err
  }
}

// function setPaymentByName (name, payment, TTL) {
//   // one user to many payments
//   client.rpush(`${name}list`, JSON.stringify(payment), client.print)
// }

// function getAll1By1OnList (name) {
//   client.lrange(`${name}list`, 0, -1, function (err, cache) {
//     for (var i in cache) {
//       return cache[i]
//     }
//   })
// }

/**
 * LREM(list, index, value)
rpush(list), rpop(list, value)
lrange for looping list (list, start, end, cb)

var arr = [1,2,3]

var multi = client.multi()

for (var i=0; i<arr.length; i++) {
  multi.rpush('testlist', arr[i]);
}

multi.exec(function(errors, results) {
  console.log('errors', errors)
  console.log('results', results)
})
client.rpop('testlist')
client.rpush('testlist', JSON.stringify({"hehe": "haha"}), client.print)
 */
