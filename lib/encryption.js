const CryptoJS = require('crypto-js')
require('dotenv').config()

const key = process.env.ENCRYPT_KEY

function encryption(data) {
  let ciphertext
  if (typeof data === 'string') {
    ciphertext = CryptoJS.AES.encrypt(data, key)
    return ciphertext.toString()
  }
  ciphertext = CryptoJS.AES.encrypt(JSON.stringify(data), key)
  return ciphertext.toString()
}

function decryption(data) {
  const bytes = CryptoJS.AES.decrypt(data.toString(), key)
  const decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8))
  return decryptedData
}

module.exports = {
  encryption,
  decryption,
}
