const admin = require('firebase-admin')
const serviceAccount = require('./hk01-511765deb921.json')
require('dotenv').config()

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.FIREBASE_URL,
  databaseAuthVariableOverride: {
    uid: process.env.FIREBASE_SERVICE_WORKER,
  },
})

const db = admin.database()

module.exports = db
