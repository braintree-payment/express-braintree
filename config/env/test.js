const supertest = require('supertest')
const { expect } = require('chai')
const api = 'http://localhost:3001' 

module.exports = {
  server: supertest.agent(api),
  apiPrefix: '/api/v1',
  expect: expect
}
