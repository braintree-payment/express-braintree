import index from '../components/index'
import payment from '../components/payment'
import braintree from '../components/payment/braintree'
import paypal from '../components/payment/paypal'

const Routes = (app) => {
  app.use('/', index)
  app.use('/api/v1/payment', payment)
  app.use('/api/v1/payment/braintree', braintree)
  app.use('/api/v1/payment/paypal', paypal)
  // catch 404 and forward to error handler
  app.use((req, res, next) => {
    const err = new Error('Not Found')
    err.status = 404
    next(err)
  })

  // error handler
  app.use((err, req, res) => {
    // set locals, only providing error in development
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}

    // render the error page
    res.status(err.status || 500)
    res.render('error')
  })
}

export default Routes