const { expect, server } = require('../../config/env/test')

describe('Index tests', () => {
  it('should return "ok"', (done) => {
    server
      .get('/')
      .expect('Content-type', 'text/html; charset=utf-8')
      .expect(200, done)
  })
})
