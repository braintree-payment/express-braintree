import express from 'express'
import * as payment from './payment.controller'

const router = express.Router()

router.get('/:id', (req, res, next) => payment.findPayment(req, res, next))

export default router
