import db from '../../config/firebase'
import { setPaymentByID, findPayment } from '../../lib/redis'

const paymentsRef = db.ref('payments')

class Payment {
  /**
  *
  * @param {string} id
  * @param {integer} amount
  * @param {string} currency
  * @param {string} payer
  * @param {string} phone
  */
  constructor(payObj) {
    const {
      id, amount, currency, payer, phone,
    } = payObj
    this._id = id
    this._amount = amount
    this._currency = currency
    this._payer = payer
    this._phone = phone
    this._createdAt = new Date()
  }

  /**
   * @return {this<object>}
   */
  getPayment(payment) {
    if (payment) {
      const { platform } = payment
      if (platform === 'braintree') {
        return {
          id: payment.id,
          amount: payment.amount,
          currency: payment.currencyIsoCode,
          payer: payment.customer.firstName,
          phone: payment.customer.phone,
          createdAt: payment.transaction.createdAt
        }
      } else if (platform === 'paypal') {
        const description = JSON.parse(payment.transactions[0].description)
        const { name, phone, prefix } = description
        const { total, currency } = payment.transactions[0].amount
        return {
          id: payment.id,
          amount: total,
          currency,
          payer: name,
          phone: `+${prefix} ${phone}`,
          createdAt: payment.create_time
        }
      }
      return payment
    }
    return {
      id: this._id,
      amount: this._amount,
      currency: this._currency,
      payer: this._payer,
      phone: this._phone,
      createdAt: this._createdAt
    }
  }

  /**
   * @return {'OK'<string>}
   */
  async save2Cache(payment) {
    if (!payment) {
      return setPaymentByID(this._id, this.getPayment(), 3600)
    }
    return setPaymentByID(this._id, payment, 3600)
  }

  /**
   * @return {_id<string>}
   */
  async save2Firebase(payment) {
    try {
      if (!payment) {
        await paymentsRef.child(this._id).set(this.getPayment())
      } else {
        await paymentsRef.child(this._id).set(payment)
      }
      return this._id
    } catch (err) {
      throw err
    }
  }

  /**
   * @return {_id<string>}
   */
  async createPayment() {
    try {
      const toCache = await this.save2Cache()
      const toFirebase = await this.save2Firebase()
      if (toCache && toFirebase === this._id) { return this._id }
    } catch (err) {
      throw err
    }
    return null
  }

  /**
   * @return {payment<object>}
   */
  async findPaymentByCache() {
    try {
      const res = await findPayment(this._id)
      return JSON.parse(res)
    } catch (err) {
      throw err
    }
  }

  /**
   * @return {payment<object>}
   */
  async findPaymentByFirebase() {
    try {
      const res = await paymentsRef.child(this._id).once('value')
      return res
    } catch (err) {
      console.log('404 from firebase')
    }
    return null
  }
}

export default Payment
