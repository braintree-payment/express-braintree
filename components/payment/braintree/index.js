import express from 'express'
import * as braintree from './braintree.controller'

const router = express.Router()

router.get('/client-token', (req, res, next) =>
  braintree.getToken(req, res, next))

router.post('/checkout', (req, res, next) =>
  braintree.createTransaction(req, res, next))

export default router
