const braintree = require('braintree')

require('dotenv').config()

const environment =
  process.env.BT_ENVIRONMENT.charAt(0).toUpperCase() +
  process.env.BT_ENVIRONMENT.slice(1)

const braintreeConfig = {
  environment: braintree.Environment[environment],
  merchantId: process.env.BT_MERCHANT_ID,
  publicKey: process.env.BT_PUBLIC_KEY,
  privateKey: process.env.BT_PRIVATE_KEY,
}

const gateway = braintree.connect(braintreeConfig)

module.exports = gateway
