import braintree from './braintree.config'
import Payment from '../payment.model'

class Braintree extends Payment {
  constructor(payObj) {
    if (payObj !== undefined) {
      // define braintree model with payobj
      const { nonce } = payObj
      super(payObj)
      this._nonce = nonce
    } else {
      // without payobj
      super({})
    }
  }

  /**
   * @param {string} id
   */
  setId(id) {
    this._id = id
    return id
  }

  /**
   * @param {datetime} datetime
   */
  setCreatedAt(datetime) {
    this._createdAt = datetime
    return datetime
  }

  /**
   * @return {token<string>}
   */
  getClientToken() {
    return braintree.clientToken.generate()
  }

  /**
   * @param {obj} details
   */
  async transaction() {
    const {
      _amount, _currency, _payer, _phone, _nonce,
    } = this
    try {
      const res = await braintree.transaction.sale({
        amount: parseFloat(_amount),
        paymentMethodNonce: _nonce,
        merchantAccountId: `hk01_${_currency.toLowerCase()}`,
        customer: {
          firstName: _payer,
          phone: _phone,
        },
        options: {
          submitForSettlement: true,
        },
      })
      return res
    } catch (err) {
      throw err
    }
  }

  async findPayment() {
    try {
      return await braintree.transaction.find(this._id)
    } catch (err) {
      console.log('404 from braintree')
    }
    return null
  }
}

export default Braintree
