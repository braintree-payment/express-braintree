/**
 * Formatting error response
 * @param {*} errors
 */
export const formatErrors = (errors) => {
  let formattedErrors = ''

  for (let i = 0; i < errors.length; i++) { // eslint-disable-line no-inner-declarations, vars-on-top
    if (errors.hasOwnProperty(i)) {
      formattedErrors += `Error: ${errors[i].code}: ${errors[i].message}\n`
    }
  }
  return formattedErrors
}
