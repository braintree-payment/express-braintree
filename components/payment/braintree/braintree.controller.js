import Braintree from './braintree.model'
import { formatErrors } from './braintree.errors'

/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @return {client token<string>}
 */
export const getToken = async (req, res) => {
  try {
    const braintree = new Braintree()
    const response = await braintree.getClientToken()
    res.send(response.clientToken)
  } catch (err) {
    res.status(err.status || 500)
    res.send(err)
  }
}

/**
 * Execte Braintree payment
 * @return {payment<object>}
 */
export const createTransaction = async (req, res) => {
  let transactionErrors
  const {
    price, payment_method_nonce, name, phone, currency, prefix,
  } = req.body
  const braintree = new Braintree({
    amount: price,
    currency,
    payer: name,
    phone: `+${prefix} ${phone}`,
    nonce: payment_method_nonce,
  })
  try {
    const result = await braintree.transaction()
    if (!result.errors) {
      braintree.setId(result.transaction.id)
      braintree.setCreatedAt(result.transaction.createdAt)
      const pay = await braintree.createPayment()
      res.send(pay)
    } else {
      transactionErrors = result.errors.deepErrors()
      res.status(500).send({
        message: formatErrors(transactionErrors),
      })
    }
  } catch (err) {
    res.status(err.status || 500)
    res.send(err)
  }
}
