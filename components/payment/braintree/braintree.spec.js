const { expect, server, apiPrefix } = require('../../../config/env/test')

const settings = {
  'client-token': {},
  'checkout': {
    'body': {
	    'prefix':'852',
      'name':'Integration Tester',
      'phone':'2345678',
      'currency':'HKD',
      'price':'2',
      'CCHN':'Integration Tester',
      'CCNum':'4111-1111-1111-1111', // visa 
      'CCV':'234',
      'payment_method_nonce':'fake-valid-nonce'
    }
  }
}

describe('Braintree tests', () => {
  describe('Braintree pay', () => {
    it('get braintree client token', (done) => {
      server
        .get(`${apiPrefix}/payment/braintree/client-token`)
        .expect('Content-type', 'text/html; charset=utf-8')
        .expect(200)
        .end((err, res) => {
          if (err) done(err)
          expect(res.text).to.be.a('string')
          done()
        })
    })

    it('create transaction', (done) => {
      server
        .post(`${apiPrefix}/payment/braintree/checkout`)
        .send(settings.checkout.body)
        .set('Accept', 'application/json')
        .expect('Content-type', 'text/html; charset=utf-8')
        .expect(200)
        .end((err, res) => {
          if (err) done(err)
          expect(res.text).to.be.a('string')
          done()
        })
    })
  })
})
