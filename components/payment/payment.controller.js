import Payment from './payment.model'
import Braintree from './braintree/braintree.model'
import Paypal from './paypal/paypal.model'
import { encryption } from '../../lib/encryption'

export const findPayment = async (req, res) => {
  let response
  const { id } = req.params
  const payment = new Payment({
    id,
  })

  const cache = await payment.findPaymentByCache()
  if (cache) {
    res.send(encryption(cache))
  } else {
    // no cache
    try {
      const braintree = new Braintree({ id })
      const paypal = new Paypal({ id })
      const firebaseResult = await payment.findPaymentByFirebase()
      const braintreeResult = await braintree.findPayment()
      const paypalResult = await paypal.findPayment()
      if (firebaseResult.val()) {
        // save cache
        response = Object.assign(firebaseResult.val(), {
          cached: await payment.save2Cache(firebaseResult.val()),
        })
        res.send(encryption(response))
      } else if (braintreeResult) {
        response = payment.getPayment(Object.assign(braintreeResult, {
          platform: 'braintree',
        }))
        res.send(encryption(Object.assign(response, {
          saved2Firebase: await payment.save2Firebase(response) === payment._id,
        }, {
            cached: await payment.save2Cache(response),
          })))
      } else if (paypalResult) {
        response = payment.getPayment(Object.assign(paypalResult, {
          platform: 'paypal',
        }))
        res.send(encryption(Object.assign(response, {
          saved2Firebase: await payment.save2Firebase(response) === payment._id,
        }, {
            cached: await payment.save2Cache(response),
          })))
      } else {
        res.status(404).send({
          name: 'Opsss...', message: 'Record cannot be found',
        })
      }
    } catch (err) {
      res.status(err.status || 500)
      res.send({
        name: `${err.status} ${err.statusText}` || 'Internal server error',
        message: err.response || 'Internal server error',
      })
    }
  }
}
