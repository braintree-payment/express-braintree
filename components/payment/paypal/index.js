import express from 'express'
import * as paypal from './paypal.controller'

const router = express.Router()

router.post('/create-payment', (req, res, next) =>
  paypal.createPayment(req, res, next))

router.post('/execute/:paymentId', (req, res, next) =>
  paypal.executePayment(req, res, next))

export default router
