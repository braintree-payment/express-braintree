import paypal from 'paypal-rest-sdk'
import { load } from 'dotenv'
import './paypal.config'
import Payment from '../payment.model'

load()

class Paypal extends Payment {
  constructor(payObj) {
    if (payObj !== undefined) {
      super(payObj)
      this._payerId = payObj.payerId ? payObj.payerId : null
    } else {
      super()
    }
  }

  /**
   * @param {array} transactions
   * @return {payment<object>}
   */
  getPaymentJson(transactions) {
    const redirectURL = process.env.PAYPAL_REDIRECT_URL
    return {
      intent: 'sale',
      payer: {
        payment_method: 'paypal',
      },
      redirect_urls: {
        return_url: redirectURL,
        cancel_url: redirectURL,
      },
      transactions,
    }
  }

  /**
   * Create payment
   * @param {*} paymentJson
   * @return {paymant details by paypal<object>}
   */
  async createPaymentByPaypal(transactions) {
    const paymentJson = this.getPaymentJson(transactions)
    return new Promise((resolve, reject) => {
      paypal.payment.create(paymentJson, (err, payment) => {
        if (err) reject(err)
        resolve(payment)
      })
    })
  }

  /**
   *
   * @param {*} paymentId
   * @param {*} payment
   * @return {payment<object>}
   */
  async setPaymentById() {
    return new Promise((resolve, reject) => {
      paypal.payment.get(this._id, (err, payment) => {
        if (err) reject(err)
        // description sting to object
        const { description } = payment.transactions[0]
        const transactionObj = JSON.parse(description)
        const {
          currency, name, phone, prefix, price,
        } = transactionObj
        this._amount = price
        this._currency = currency
        this._payer = name
        this._phone = `+${prefix} ${phone}`
        this._createdAt = payment.create_time
        resolve(payment)
      })
    })
  }

  /**
   *
   * @param {array} transactions
   * @return {execution config<object>}
   */
  getExecutionConfig(transactions) {
    delete transactions[0].description
    return {
      payer_id: this._payerId,
      transactions,
    }
  }

  /**
   * @param {object} payment
   * @return {result<object>}
   */
  async executePayment(payment) {
    return new Promise((resolve, reject) => {
      paypal.payment.execute(
        this._id,
        this.getExecutionConfig(payment.transactions),
        (err, result) => {
          if (err) reject(err)
          resolve(result)
        },
      )
    })
  }

  findPayment() {
    return new Promise(resolve =>
      paypal.payment.get(this._id, (err, result) => {
        if (err) console.log('404 from paypal')
        resolve(result)
      }))
  }
}

export default Paypal
