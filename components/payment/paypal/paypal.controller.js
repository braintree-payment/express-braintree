import Paypal from './paypal.model'

export const createPayment = async (req, res) => {
  try {
    const { total, currency } = req.body.transactions[0].amount
    const paypal = new Paypal({
      amount: total,
      currency,
    })
    const payment = await paypal.createPaymentByPaypal(req.body.transactions)
    res.send(payment)
  } catch (err) {
    res.status(err.status || 500)
    res.send(err)
  }
}

export const executePayment = async (req, res) => {
  try {
    const paypal = new Paypal({
      id: req.params.paymentId,
      payerId: req.body.payer_id,
    })
    const payment = await paypal.setPaymentById()
    const execution = await paypal.executePayment(payment)
    const result = await paypal.createPayment(execution)
    res.send(result)
  } catch (err) {
    res.status(err.status || 500)
    res.send(err)
  }
}
